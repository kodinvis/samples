WITH
popular_tags as
(
    SELECT t.o2t_tag_obj_id, t.o2t_tag_name, SUM(t.posts_num) as posts_num, SUM(t.groups_num) as groups_num
    FROM
        (
            (
                SELECT o2t.o2t_tag_obj_id, LOWER(o2t.o2t_tag_name) o2t_tag_name, COUNT(DISTINCT o2t.o2t_obj_obj_id) as posts_num, COUNT(DISTINCT bp.bgpost_blog_obj_id) as groups_num
                FROM blog_post bp
                         JOIN obj2tag o2t ON o2t.o2t_obj_obj_id = bp.obj_id
                WHERE bp.obj_status_did = 1 AND bp.obj_created > now() - interval '7 days'
                  AND bp.msg_type_did = 69000001111
                  AND EXISTS (SELECT 1 FROM "group" WHERE "group".obj_id = bp.bgpost_blog_obj_id AND "group".group_data->'state' = '1')
                GROUP BY o2t.o2t_tag_obj_id, o2t.o2t_tag_name
            )
            UNION
            (
                SELECT o2t.o2t_tag_obj_id, LOWER(o2t.o2t_tag_name) o2t_tag_name, COUNT(DISTINCT o2t.o2t_obj_obj_id) as posts_num, 0 as groups_num
                FROM user_post up
                         JOIN obj2tag o2t ON o2t.o2t_obj_obj_id = up.obj_id
                WHERE up.obj_status_did = 1 AND up.obj_created > now() - interval '7 days'
                GROUP BY o2t.o2t_tag_obj_id, o2t.o2t_tag_name
            )
        ) t
    GROUP BY t.o2t_tag_obj_id, t.o2t_tag_name
    ORDER BY posts_num DESC
)

SELECT pt.* FROM popular_tags pt
JOIN tag t ON t.obj_id = pt.o2t_tag_obj_id
WHERE NOT coalesce(t.tag_is_blacklisted, false)
LIMIT 10
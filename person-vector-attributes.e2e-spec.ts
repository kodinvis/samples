import { config } from '../src/config';
import RequestUtility from './utils/request.utility';
import { ObjUtility } from '../src/common/utils/obj.utility';
import { VectorAttribute } from '../src/person-vector-attributes/types/vector-attribute';

jest.setTimeout(config.tests.timeout);

describe('E2E Person Vector Attributes Tests', () => {
  const testTagId = 80000339079;

  it('should add vector attribute for current user', async () => {
    const res = await RequestUtility.makeTestRequest(globalThis.httpServer,
      'addVectorAttribute',
      {attributeType: ObjUtility.getKeyByValue(VectorAttribute, VectorAttribute.TAG), attributeId: testTagId, prefers: true},
      [{name: 'Authorization', value: 'Bearer ' + globalThis.jwt}]
    );
    expect(res).toBeTruthy();
  });

  it('should get vector attributes for current user', async () => {
    const res = await RequestUtility.makeTestRequest(globalThis.httpServer,
      'getVectorAttributes',
      {},
      [{name: 'Authorization', value: 'Bearer ' + globalThis.jwt}]
    );
    expect(res.length).toBeGreaterThan(0);
    expect(res[0].pva_person_id).toEqual(config.tests.userId);
    let testTagFound = false;
    res.forEach(item => {
      if (item.pva_attribute_id == testTagId) {
          testTagFound = true;
          return;
      }
    })
    expect(testTagFound).toBeTruthy();
  });

  it('should get vector attributes for specified user', async () => {
    const res = await RequestUtility.makeTestRequest(globalThis.httpServer,
      'getUserVectorAttributes',
      {userId: config.tests.userId},
      [{name: 'Internal-Key', value: config.internalKey}]
    );
    expect(res.length).toBeGreaterThan(0);
    expect(res[0].pva_person_id).toEqual(config.tests.userId);
  });

  it('should not get vector attributes for specified user without internal key', async () => {
    const res = await RequestUtility.makeTestRequest(globalThis.httpServer,
      'getUserVectorAttributes',
      {userId: config.tests.userId}
    );
    expect(res).toBeNull();
  });

  it('should delete vector attribute for current user', async () => {
    const res = await RequestUtility.makeTestRequest(globalThis.httpServer,
      'deleteVectorAttribute',
      {attributeType: ObjUtility.getKeyByValue(VectorAttribute, VectorAttribute.TAG), attributeId: testTagId},
      [{name: 'Authorization', value: 'Bearer ' + globalThis.jwt}]
    );
    expect(res).toBeTruthy();
  });
});

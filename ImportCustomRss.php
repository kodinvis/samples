<?php

/**
 * @author Oleg Kravchenko
 * @example misc/tools/runner.php --tool=ImportCustomRss -v
 */
class Cronjob_Tool_ImportCustomRss extends Cronjob\Tool\ToolBase
{
    const LAST_ADDED_ITEM_CACHE_KEY_PREFIX = 'CUSTOM_RSS_LAST_ADDED_ITEM_';
    const EXISTING_ITEM_HASHES_CACHE_KEY_PREFIX = 'CUSTOM_RSS_EXISTING_ITEM_HASHES_';

    static function getCommandLineSpec()
    {
        return array(
            'dry-run' => array(
                'short' => 'd',
                'desc' => 'do not actually create/update blogposts',
                'valueRequired' => false
            )
        );
    }

    /**
     * @param array $item One rss item data
     * @return array Prepared array for saving into db
     */
    private function prepareBlogPostProto($item)
    {
        $published = isset($item['published']) ? $item['published'] : false;

        $proto = array();
        $proto['type_did'] = Dictionary::getInstance()->getId(Mapper_BlogPost::BLOG_POST_TYPE_GENERIC);
        $proto['status_did'] = IHasStatus::STATUS_ACTIVE;
        $proto['notification_status_did'] = IHasStatus::STATUS_PENDING;
        $proto['vislvl_allow_comments'] = Mapper::VISLVL_ANY;

        $html_meta = [];
        if ($published !== false) {
            $html_meta['rss_date'] = $published;
        }
        if (!empty($item['link'])) {
            $html_meta['rss_source'] = $item['link'];
            $html_meta['canonical'] = $item['link'];

            $path = URL::c($item['link'])->getPath();
            if ($path) {
                $proto['string_id'] = mb_substr($path, 1);
            }
        }
        $proto['html_meta'] = $html_meta;
        $proto['title'] = empty($item['title']) ? Dictionary::getInstance()->getWord('blogpost_untitled') : $item['title'];
        $proto['photoUrl'] = (empty($item['photoUrl'])) ? null : $item['photoUrl'];
        $text = empty($item['content']) ? (empty($item['description']) ? '' : trim($item['description'])) : trim($item['content']);
        $proto['text'] = Parser_Feed::prepareText($text, $item['link'], $proto['title']);
        $proto['safe_html'] = false;
        $proto['approved'] = Utils_Date::getTimestampWithMicrosecondsInPgFormat(time());

        return $proto;
    }

    /**
     * @param string $rssUrl
     * @return array Rss items on success or empty array otherwise
     */
    private function getRssItems($rssUrl)
    {
        $config = Config::getInstance();
        $content = Utils_Filesystem::fetchRemoteFileWithTimeout($rssUrl, $config->remote_timeout, $config->remote_obj_max_file_size);
        if (empty($content)) {
            $this->debugLogger->error('Empty rss feed');
            return [];
        }
        $parser = Parser_Feed::process($this->debugLogger, $content);
        $items = $parser->getItems();
        $this->debugLogger->message('Parsed items: ' . count($items));

        // Sort items by published
        uasort($items, function($a, $b){ return $a['published'] > $b['published']; });

        return $items;
    }

    /**
     * @param int $siteId
     * @return string
     */
    private function getLastAddedItemCacheKey($siteId)
    {
        return self::LAST_ADDED_ITEM_CACHE_KEY_PREFIX . $siteId;
    }

    /**
     * @param int $siteId
     * @return bool|mixed
     */
    private function getLastAddedItem($siteId)
    {
        $kvdpp = CoreLight::getInstance()->getServiceBaseCacheKvdpp();
        $cacheKey = $this->getLastAddedItemCacheKey($siteId);

        return $kvdpp->get($cacheKey);
    }

    /**
     * @param int $siteId
     * @param array $item
     */
    private function setLastAddedItem($siteId, $item)
    {
        $kvdpp = CoreLight::getInstance()->getServiceBaseCacheKvdpp();
        $cacheKey = $this->getLastAddedItemCacheKey($siteId);
        $kvdpp->set($cacheKey, $item);
    }

    /**
     * Save the first new post from specified rss items
     * @param array $rssItems
     * @param Entity_Group $eGroup Site for which need to save a new post
     * @param bool $dryRun If true - skip actual saving
     * @return int|null Saved post id or null on failure
     */
    private function savePostFromRssItems($rssItems, Entity_Group $eGroup, $dryRun)
    {
        if (empty($rssItems)) {
            return;
        }
        $cache = Memcached_Manager::getInstance();
        $existingHashesCacheKey = self::EXISTING_ITEM_HASHES_CACHE_KEY_PREFIX . $eGroup->id;
        $existingHashes = $cache->get($existingHashesCacheKey);
        if (!$existingHashes) {
            $existingHashes = [];
        }
        $Mapper_GroupForeignBlogPost = Mapper_GroupForeignBlogPost::getInstance();

        foreach ($rssItems as $item) {

            $this->debugLogger->message('item:'.$item['title'] . '--'.$item['published']);
            
            // Check for existing hash
            if (in_array($item['hash'], $existingHashes)) {
                $this->debugLogger->message('skip post - hash exists in cache');
                continue;
            }
            // Check if foreign blog post already exists
            if ($Mapper_GroupForeignBlogPost->existsAggregateInDb([
                'group_id' => $eGroup->id,
                'hash' => $item['hash']
            ])) {
                // Save existing hash into the cache
                $existingHashes[] = $item['hash'];
                $cache->set($existingHashesCacheKey, $existingHashes);

                $this->debugLogger->message("skip post - hash exists in db");
                continue;
            }
            // Prepare proto
            $proto = $this->prepareBlogPostProto($item);

            if ($dryRun) {
                $this->debugLogger->message("dry run: must be added with proto " . var_export($proto,true));
                return;
            }

            // Add new item
            $transactionStarted = \DB::beginTransactionSmart(null, $savePointName = __FUNCTION__ . get_class($this) . microtime(true));
            try {
                $blogPostId = $eGroup->addBlogPost($proto, $eGroup->person_id, false);

                // add foreign blog post entity
                $Mapper_GroupForeignBlogPost->createEntity(array(
                    'blog_post_id' => $blogPostId,
                    'group_id' => $eGroup->id,
                    'hash' => $item['hash'],
                    'content_hash' => $item['content_hash']
                ));
                \DB::commitTransactionSmart(null, $transactionStarted);

                // update the cache - last added item and existing hashes
                $this->setLastAddedItem($eGroup->id, ['published' => $item['published'], 'created' => time()]);
                $existingHashes[] = $item['hash'];
                $cache->set($existingHashesCacheKey, $existingHashes);

                $this->debugLogger->message("Added new post '{$item['title']}' with id $blogPostId");

                return $blogPostId;

            } catch (\Exception $e) {
                \DB::rollbackTransactionSmart(null, $transactionStarted, $savePointName);
                $this->debugLogger->error("Post '{$item['title']}' not added." . PHP_EOL . $e->getMessage());
                continue;
            }
        }
    }

    /**
     * @see Cronjob\Tool\ITool::run()
     */
    function run(ICronjob $cronjob)
    {
        $config = Config::getInstance();
        $dryRun = (bool)$cronjob->getOption('dry-run');
        $customRssConfig = $config->custom_rss;

        foreach ($customRssConfig as $customRssSiteConfig) {

            // Get site by id
            $eGroup = Mapper_Group::getInstance()->getEntityFromDB(['id' => $customRssSiteConfig['site_id']]);
            if (empty($eGroup)) {
                $this->debugLogger->error("Site {$customRssSiteConfig['site_id']} not found");
                continue;
            }

            // Get last added item time to check the delay
            $lastAddedItem = $this->getLastAddedItem($customRssSiteConfig['site_id']);
            $this->debugLogger->message("Last added item " . var_export($lastAddedItem, true));

            // Check for delay
            if (!empty($customRssSiteConfig['delay']) && !empty($lastAddedItem['created'])
                && ($secs = (time() - $lastAddedItem['created'])) < $customRssSiteConfig['delay']) {
                $this->debugLogger->message("$secs seconds passed after last added item. Wait for delay {$customRssSiteConfig['delay']}");
                continue;
            }

            $page = 1;
            $pageQueryStr = '';
            do {
                // Fetch rss items
                $rssUrl = $customRssSiteConfig['rss'] . $pageQueryStr;
                $this->debugLogger->message("Start parsing rss $rssUrl for site {$customRssSiteConfig['site_id']}");
                $items = $this->getRssItems($rssUrl, $customRssSiteConfig['site_id']);

                // Add new post from the items
                $newPostId = $this->savePostFromRssItems($items, $eGroup, $dryRun);
                if (!$newPostId) {
                    if (!empty($customRssSiteConfig['page_params_tpl']) && !empty($customRssSiteConfig['max_page_attempts'])) {
                        $page++;
                        $pageQueryStr = sprintf($customRssSiteConfig['page_params_tpl'], $page);
                    }
                }
            } while(!$newPostId && $pageQueryStr && $page <= $customRssSiteConfig['max_page_attempts']);
            
            if (empty($newPostId)) {
                $this->debugLogger->message('No items added');
            }
        }
    }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PersonVectorAttribute } from './person-vector-attribute.entity';
import { Args } from '@nestjs/graphql';
import { AddPersonVectorAttributeDto } from './dto/add-person-vector-attribute.dto';
import { AuthService } from '../auth/auth.service';
import { DeletePersonVectorAttributeDto } from './dto/delete-person-vector-attribute.dto';
import { PersonalizedService } from '../common/types/personalized.service';
import { CacherService } from '../cacher/cacher.service';

@Injectable()
export class PersonVectorAttributesService extends PersonalizedService {

  private CACHE_TTL = 86400;

  constructor(
    @InjectRepository(PersonVectorAttribute) private personVectorAttributesRepository: Repository<PersonVectorAttribute>,
    authService: AuthService,
    private cacherService: CacherService,
  ) {
    super(authService);
  }

  async getByUserId(userId: number): Promise<PersonVectorAttribute[]> {
    const cacheKey = this.getCacheKey(userId);
    let personVectorAttributes: PersonVectorAttribute[] = await this.cacherService.get(cacheKey);
    if (personVectorAttributes) {
        return personVectorAttributes;
    }
    personVectorAttributes = (userId) ? await this.personVectorAttributesRepository.findBy({pva_person_id: userId}) : [];
    await this.cacherService.set(cacheKey, personVectorAttributes, this.CACHE_TTL);

    return personVectorAttributes;
  }

  async get(): Promise<PersonVectorAttribute[]> {
    return (this.currentUserId) ? await this.getByUserId(this.currentUserId) : [];
  }

  async addVectorAttribute(@Args() args: AddPersonVectorAttributeDto): Promise<boolean> {
    if (!this.currentUserId) {
        return false;
    }
    const personVectorAttribute = new PersonVectorAttribute();
    personVectorAttribute.pva_person_id = this.currentUserId;
    personVectorAttribute.pva_attribute_type = args.attributeType;
    personVectorAttribute.pva_attribute_id = args.attributeId;
    personVectorAttribute.pva_prefers = args.prefers;

    const res = await this.personVectorAttributesRepository
      .createQueryBuilder()
      .insert()
      .values(personVectorAttribute)
      .onConflict('ON CONSTRAINT pk_person_vector_attribute DO NOTHING')
      .execute()
    ;
    if (res && res.identifiers.length) {
      await this.cacherService.delete(this.getCacheKey(this.currentUserId));
      return true;
    } else {
      return false;
    }
  }

  async deleteVectorAttribute(@Args() args: DeletePersonVectorAttributeDto): Promise<boolean> {
    if (!this.currentUserId) {
      return false;
    }
    const res = await this.personVectorAttributesRepository.delete({
      pva_person_id: this.currentUserId,
      pva_attribute_type: args.attributeType,
      pva_attribute_id: args.attributeId
    });
    if (res && res.affected > 0) {
      await this.cacherService.delete(this.getCacheKey(this.currentUserId));
      return true;
    } else {
      return false;
    }
  }

  private getCacheKey(userId: number): string {
    return this.cacherService.CACHE_PREFIX_VECTOR_ATTRIBUTES + userId;
  }
}

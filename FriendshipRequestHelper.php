<?php

/**
 * Description
 * Helper to manage friendship requests for the user
 *
 * Author: Oleg Kravchenko
 */

class FriendshipRequestHelper
{
    const FRIENDSHIP_REQUEST_TYPE_INCOME = 'income';
    const FRIENDSHIP_REQUEST_TYPE_OUTCOME = 'outcome';

    /**
     * Gets incoming and outgoing friendship requests user ids for specified person
     * @param int $personId
     * @return array
     */
    private static function getFriendRequests($personId)
    {
        $sql = "
            SELECT t.talk_person_obj_ids, a.msg_from_person_obj_id
            FROM action a
            JOIN talk t ON t.obj_id = a.msg_conversation_obj_id
            WHERE t.talk_person_obj_ids @> array[$personId::bigint]
            AND a.obj_status_did=" . ServiceBase_IHasStatus::STATUS_PENDING . "
            AND a.msg_type_did=" . Dictionary::getInstance()->getId(Mapper_Action::ACTION_TYPE_JOIN_REQUEST_MESSAGE) . "
            ORDER BY a.obj_created DESC
        ";
        $stmt = \DB::getConnection('DSN_DB2')->prepareStatement($sql);

        return $stmt->executeQuery();
    }

    /**
     * Prepare users by ids
     * @param int $personId
     * @param array $userIds
     * @return array
     */
    private static function prepareUsers($personId, $userIds)
    {
        $users = [];
        $userIds = array_unique($userIds);
        $aUsers = Mapper_Person::getInstance()->getAggregateFromDB(['id' => $userIds])
            ->filter(function($eUser) use ($personId) {
                return !$eUser->revokedForPerson($personId, Mapper_Allow::ALLOW_WRITE);
            })->sortByArray('id', $userIds);
        foreach ($aUsers as $eUser) {
            $users[] = EntityHelper::makePersonArrayTiny($eUser);
        }
        return $users;
    }

    /**
     * Gets incoming and outgoing friendship requests for specified user
     * @param int $personId
     * @return array
     */
    public static function get($personId = null)
    {
        $res = [];
        if (is_null($personId)) {
            if (!Authentication::isAuthenticated()) {
                return $res;
            }
            $ePerson = Authentication::getPerson();
            $personId = $ePerson->id;
        } else {
            $ePerson = Mapper_Person::getInstance()->getEntityFromDB(['id' => $personId]);
            if (!$ePerson) {
                return $res;
            }
        }
        $friendIds = $ePerson->getFriendsId();
        $ignoredIds = $ePerson->getIgnoredPersonIds();
        $dbRes = self::getFriendRequests($personId);

        // Split friend requests to incoming and outgoing
        foreach ($dbRes as $item) {
            $personIds = Utils_Array::fromPgArray($item['talk_person_obj_ids']);
            if ($item['msg_from_person_obj_id'] == $personId) {
                $targetPersonId = array_values(array_diff($personIds, [$personId]))[0];
                if (!in_array($targetPersonId, $friendIds) && !in_array($targetPersonId, $ignoredIds)) {
                    $res[self::FRIENDSHIP_REQUEST_TYPE_OUTCOME][] = $targetPersonId;
                }
            } elseif (!in_array($item['msg_from_person_obj_id'], $friendIds) && !in_array($item['msg_from_person_obj_id'], $ignoredIds)) {
                $res[self::FRIENDSHIP_REQUEST_TYPE_INCOME][] = $item['msg_from_person_obj_id'];
            }
        }
        // Prepare the result
        $incomeUsers = (!empty($res[self::FRIENDSHIP_REQUEST_TYPE_INCOME]))
            ? self::prepareUsers($personId, $res[self::FRIENDSHIP_REQUEST_TYPE_INCOME]) : [];
        $outcomeUsers = (!empty($res[self::FRIENDSHIP_REQUEST_TYPE_OUTCOME]))
            ? self::prepareUsers($personId, $res[self::FRIENDSHIP_REQUEST_TYPE_OUTCOME]) : [];
        $result = [
            0 => [
                'type' => self::FRIENDSHIP_REQUEST_TYPE_INCOME,
                'items' => $incomeUsers
            ],
            1 => [
                'type' => self::FRIENDSHIP_REQUEST_TYPE_OUTCOME,
                'items' => $outcomeUsers
            ]
        ];
        return $result;
    }

    /**
     * Sets friendship automatically for users with mutual friendship requests
     * @param int $personId
     * @return int Accepted friendship number
     */
    public static function setAutoFriendship($personId = null)
    {
        $friendRequests = self::get($personId);
        
        // Search intersection in the income and outcome friendship requests
        $userIdsByType = [];
        foreach ($friendRequests as $friendRequestsByType) {
            $userIdsByType[] = Utils_Array::collectByKey($friendRequestsByType['items'], 'id');
        }
        $userIdsForAutoFriendship = array_intersect($userIdsByType[0], $userIdsByType[1]);
        $actionSystem = \Registry::getInstance()->getActionsSystem();
        $acceptedFriendshipNum = 0;
        foreach ($userIdsForAutoFriendship as $userIdForAutoFriendship) {
            if (($status = $actionSystem->getRelationshipStatus($personId, $userIdForAutoFriendship)) && $status->getActionId()) {
                $action = $actionSystem->getActionById($status->getActionId(), $status->getAction()->getTalkId());
                if ($action->getTypeKey() == 'action_type_join_request_message') {
                    $actorPersonId = ($action->getFromPersonId() == $personId) ? $userIdForAutoFriendship : $personId;
                    $actionSystem->acceptFriendshipRequest($actorPersonId, $action);
                    $acceptedFriendshipNum++;
                }
            }
        }
        return $acceptedFriendshipNum;
    }
}

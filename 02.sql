SELECT pgq.create_queue('nude_detect'),
pgq.register_consumer('nude_detect', 'nude_detect_consumer');

CREATE TABLE pgq_event_nude_detect
(
  obj_id bigserial NOT NULL,
  pend_file_obj_id bigint,
  pend_photo_location VARCHAR(255),
  CONSTRAINT pk_pgq_event_nude_detect PRIMARY KEY (obj_id)
)
WITH (
OIDS=FALSE
);

CREATE TRIGGER t_insert_pgq_event_nude_detect
BEFORE INSERT
ON pgq_event_nude_detect
FOR EACH ROW
EXECUTE PROCEDURE pgq_queue_distributor_triga('', 'nude_detect', 'SKIP', 'obj_id');


CREATE OR REPLACE FUNCTION public.t_file_nude_detect()
  RETURNS trigger AS
  $$
-- Trigger function to detect nude inside image
BEGIN
  IF (NEW.file_type_did = 69000002263 OR NEW.file_type_did = 69000021362) THEN
    INSERT INTO pgq_event_nude_detect (pend_file_obj_id, pend_photo_location)
    VALUES (NEW.obj_id, NEW.photo_location);
  END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

COMMENT ON FUNCTION public.t_file_nude_detect()
IS 'Trigger function to detect nude inside image';

CREATE TRIGGER t_file_nude_detect_a
AFTER INSERT
ON public."file"
FOR EACH ROW
EXECUTE PROCEDURE public.t_file_nude_detect();